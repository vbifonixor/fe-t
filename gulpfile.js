var gulp = require('gulp');
var sass = require('gulp-sass');
var connect = require('gulp-connect');
var csso = require('gulp-csso');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');
var pug = require('gulp-pug');
var changed = require('gulp-changed');

gulp.task('connect', function () {
	connect.server({
		root: '',
		livereload: true
	});
});


gulp.task('sass', function () {
	gulp.src('src/scss/**/*.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}).on('error', sass.logError))
		.pipe(csso({
            sourceMap: true
        }))
		.pipe(gulp.dest('app/css'))
		.pipe(connect.reload());
});

gulp.task('html', function () {
	gulp.src('./*.html')
	.pipe(connect.reload());
});

gulp.task('images', function () {
		gulp.src('src/img/**/*')
   .pipe(changed('app/img/'))
   .pipe(imagemin())
   .pipe(gulp.dest('app/img/'))
	 .pipe(connect.reload());
})

gulp.task('js', function () {
	gulp.src('src/js/*.js')
		.pipe(gulp.dest('app/js/'))
		.pipe(connect.reload());
});

gulp.task('pug', function() {
	gulp.src('src/*.pug')
	.pipe(pug())
	.pipe(gulp.dest(''))
	.pipe(connect.reload());
	gulp.src('src/pug/**/*.pug')
	.pipe(connect.reload());
})

gulp.task('watch', function () {
	gulp.watch(['*.html'], ['html']);
	gulp.watch(['src/img/**/*'], ['images']);
	gulp.watch(['src/**/*.pug'], ['pug']);
	gulp.watch(['src/scss/**/*.scss'], ['sass']);
	gulp.watch(['src/css/*.css'], ['csso']);
	gulp.watch(['src/js/**/*.js'], ['js']);
});



gulp.task('serve', ['connect', 'sass', 'pug', 'html', 'images', 'js', 'watch']);
gulp.task('default', ['sass', 'images', 'js', 'pug']);
